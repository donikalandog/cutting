# [eng] **Cutting** / [rus] **Раскрой**

### [eng] What is the project ? / [rus] Что из себя представляет проект ? 


**[English]**
The project aims to create an Android application for mobile,  
which will help the user quickly determine how much fabric  
they need in order to make cutting napkins, pillowcases,  
holders and much more.

**[Russian]**
Проект направлен на создание Android приложение для мобильных,  
которое поможет пользователем быстро определить сколько им понадобиться ткани  
для того, что бы сделать раскрой салфетки, наволочки, прихватки и многое другое.

